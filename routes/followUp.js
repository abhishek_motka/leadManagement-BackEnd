var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');

var FollowUp = require('../models/Followup');
var Lead = require('../models/Lead');

var router = express.Router();

//Only logged in admin can access these routes

//Route to add new Follow Up
router.post('/add', passport.authenticate('jwt', {session: false}), function(req, res, next){

	//Create FollowUp object from the request body
	var newFollowUp = new FollowUp({
		created: new Date().getTime(),
		lead: req.body.leadid,
		date: req.body.date,
		assignment: req.body.assignment,
		userid: req.body.userid,
		status: req.body.status,
		detail: req.body.detail
	});

	//Check if follow up with same date exists
	FollowUp.getFollowUpByDate(newFollowUp.date, function(err, followUp){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If follow up exists, addition fails
		if(followUp && followUp.lead === req.body.leadid) {
			return res.json({success: false, msg: 'Follow up with same date already exists.'});
		}

		//If everything is ok then try to add the follow up
		FollowUp.addFollowUp(newFollowUp, function(err, followUp) {
			res.type('application/json');

			//Failed due to server error
			if(err){
				return res.json({success: false, msg:'Failed to add new follow up. Same follow up may exist.'});
			}else{
				//Successfully added
				return res.json({success: true, msg: 'New Follow Up Added.', followUp: followUp});
			}
		});
	});
});

//Route to update follow up
router.post('/update', passport.authenticate('jwt', {session: false}), function(req, res, next) {

	//If updateDate doesn't exist then can't update the follow up
	if(!req.body.id) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newFollowUp = {};
	var id = req.body.id;

	//Create new object with request body parameters
	if(req.body.date)
		newFollowUp.date = req.body.date;
	if(req.body.status)
		newFollowUp.status = req.body.status;
	if(req.body.assignment)
		newFollowUp.assignment = req.body.assignment;
	if(req.body.assignment)
		newFollowUp.userid = req.body.userid;
	if(req.body.detail !== undefined)
			newFollowUp.detail = req.body.detail;

	//Update the follow up in the database using a model
	FollowUp.updateFollowUp(id, newFollowUp, function(err, updatedFollowUp){
		if(err) {
			return res.json({success: false, msg: "Something went wrong. Same follow up may exist."});
		}

		if(!updatedFollowUp){
			return res.json({success: false, msg: 'Error in updating the follow up.'});
		}else{
			return res.json({success: true, msg: 'Follow up updated successfully.', followUp: updatedFollowUp});
		}
	});
});

router.get('/getByLead', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	if(!req.query.id)
		return res.json({success: false, msg: "Invalid Request"});

	Lead.getLeadById(req.query.id, function(err, lead){
		if(err)
			return res.status(500).json({success: false, msg: "Server Error"});

		if(lead){
			if(req.user.type === 'admin' || req.user._id === lead.userid){
				FollowUp.getFollowUpsByLead(req.query.id, function(err, followups) {
					if(err)
						return res.status(500).json({success: false, msg: "Server Error"});
					else
						return res.json({success: true, followups: followups});
				});
			}else{
				return res.json({success: false, msg: "You are unauthorized to access these details"});
			}
		}else{
			return res.json({success: false, msg: "No lead exists with given id"});
		}
	})
});

//Route to get follow up by date
router.get('/get', passport.authenticate('jwt', {session: false}), function(req, res, next){

	if(!req.query.date){
		return res.json({success: false, msg: "Invalid request."});
	}

	var date = req.query.date;

	//Get follow up by date from database
	FollowUp.getFollowUpByDate(date, function(err, followUp){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!followUp){
			return res.json({success: false, msg: "Follow Up not found."});
		}else{
			return res.json({success: true, followUp: followUp});
		}
	});
});

//Route to get follow up by id
router.get('/getById', passport.authenticate('jwt', {session: false}), function(req, res, next){

	if(!req.query.id){
		return res.json({success: false, msg: "Invalid request."});
	}

	var id = req.query.id;

	//Get follow up by id from database
	FollowUp.getFollowUpById(id, function(err, followUp){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!followUp){
			return res.json({success: false, msg: "Follow Up not found."});
		}else{
			return res.json({success: true, followUp: followUp});
		}
	});
});

//Route to get all follow ups
router.get('/getAll', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	//Get all follow ups from the database
	FollowUp.getFollowUps(function(err, followUps) {
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!followUps) {
			return res.json({success: false, msg: "No follow up Found."});
		}else{
			return res.json({success: true, followUps: followUps});
		}
	});
});

//Route to delete follow up from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), function(req, res, next){

	var id = req.body.id;

	FollowUp.deleteFollowUp(id, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Follow Up."});
		else{
			return res.json({success: true, msg:"Follow Up deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), function(req, res, next) {
		if(!req.body.id)
			return res.json({success: false, msg: "Invalid Request"});
		FollowUp.reset(req.body.id, function(err) {
			if(err){
				return res.json({success: false, msg: "Error in reseting Follow Ups."});
			}else{
				return res.json({success: true, msg: "All data of followups is reset."});
			}
		})
});

//delete all follow ups
router.delete('/remove', passport.authenticate('jwt', {session: false}), function(req, res, next) {
		Lead.getLeads(function(err, leads) {
			if(err)
				return res.json({success: false, msg: "Error in resetting followUps"});

			if(leads.length > 0)
				return res.json({success: false, msg: "There should be no leads to remove all follow ups"});

			FollowUp.removeAll(function(err) {
				if(err)
					return res.json({success: false, msg: "Error in resetting followups"});

				return res.json({success: true, msg: "All followups data is reset"});
			})
		})
});

module.exports = router;

var mongoose = require('mongoose');

var RequirementSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true,
		dropDups: true
	},
	status: {
		type: Boolean,
		required: true,
		default: true
	}
});

var Requirement = mongoose.model('Requirement', RequirementSchema);

module.exports = Requirement;

module.exports.addRequirement = function(requirement, callback) {
	requirement.save(callback);
};

module.exports.getRequirementById = function(id, callback) {
	Requirement.findById(id, callback);
};

module.exports.getRequirementByName = function(name, callback) {
	var query = {name: name};
	Requirement.findOne(query, callback);
};

module.exports.deleteRequirement = function(name, callback) {
	var query = {name: name};
	Requirement.findOneAndRemove(query, callback);
};

module.exports.updateRequirement = function(name, newRequirement, callback) {
	var query = {name: name};
	Requirement.findOneAndUpdate(query, newRequirement, callback);
};

module.exports.getRequirements = function(callback) {
	Requirement.find(callback);
};

module.exports.reset = function(callback) {
	Requirement.remove({}, callback);
};

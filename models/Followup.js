var mongoose = require('mongoose');
var Status = require('../models/Status');
var User = require('../models/user');

var FollowUpSchema = mongoose.Schema({
	lead: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		required: true
	},
	date: {
		type: Date,
		required: true,
		default: new Date().getTime()
	},
	status: {
		type: "String",
		required: true
	},
	assignment: {
		type: "String",
		required: true
	},
	userid: {
		type: String,
		required: true
	},
	detail: {
		type: String,
		max: 300
	}
});

var FollowUp = mongoose.model('FollowUp', FollowUpSchema);

module.exports = FollowUp;

module.exports.addFollowUp = function(followUp, callback) {
	followUp.save(callback);
};

module.exports.getFollowUpById = function(id, callback) {
	FollowUp.findById(id, callback);
};

module.exports.getFollowUpsByLead = function(leadId, callback) {
	var query = {lead: leadId};
	FollowUp.find(query, callback);
};

module.exports.getFollowUpByDate = function(date, callback) {
	var query = {date: date};
	FollowUp.findOne(query, callback);
};

module.exports.getFollowUpByAssignment = function(id, callback) {
	var query = {userid: id};
	FollowUp.findOne(query, callback);
};

module.exports.deleteFollowUp = function(id, callback) {
	var query = {_id: id};
	FollowUp.findOneAndRemove(query, callback);
};

module.exports.updateFollowUp = function(id, newFollowUp, callback) {
	var query = {_id: id};
	FollowUp.findOneAndUpdate(query, newStatus, callback);
};

module.exports.getFollowUps = function(callback) {
	FollowUp.find(callback);
};

module.exports.reset = function(id, callback) {
	var query = {lead: id};
	FollowUp.remove(query, callback);
};

module.exports.removeAll = function(callback) {
	FollowUp.remove({}, callback);
};

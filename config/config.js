module.exports = {
  CONFIRM_PASS_URL: "https://localhost:3000/#/forgotpassword/reset?check=",
  EMAIL_ADDR: 'pass-reset@mobiosolutions.com',
  EMAIL_NAME: "Password Reset",
  RESET_LINK_NAME: '"Reset Password Link"',
  SENDGRID_TEMPLATE_ID: 'f6afb58b-62c3-478c-bdc7-efd1b26f89f3'
};

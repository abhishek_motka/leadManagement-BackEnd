var mongoose = require('mongoose');

var Source = require('./Source');
var User = require('./user');
var Status = require('./Status');
var Requirement = require('./Requirement');

var LeadSchema = mongoose.Schema({
	created: {
		type: Date,
		required: true,
		default: new Date().getTime()
	},
	fName: {
		type: String,
		required: true
	},
	lName: {
		type: String,
		required: true
	},
	mobile: {
		type: String,
		required: true,
		max: 10,
		min: 10
	},
	address: {
		type: String,
		required: true,
		max: 300
	},
	source: {
		type: String,
		required: true
	},
	assignment: {
		type: String,
		required: true
	},
	userid: {
		type: String,
		required: true
	},
	status: {
		type: String,
		required: true
	},
	requirement: {
		type: String,
		required: true
	},
	notes: {
		type: String,
		required: true
	},
	nextDate: {
		type: Date,
		required: true
	},
	followUps: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: "FollowUp",
		required: true
	}]
});

var Lead = mongoose.model('Lead', LeadSchema);

module.exports = Lead;

module.exports.addLead = function(lead, callback) {
	lead.save(callback);
};

module.exports.getLeadById = function(id, callback) {
	Lead.findById(id).populate({
		path: 'followUps',
		select: 'date'
	}).exec(callback);
};

module.exports.getLeadsByUser = function(id, callback) {
	var query = {userid: id};
	Lead.find(query).populate({
		path: 'followUps',
		select: 'date'
	}).exec(callback);
};

module.exports.getLeadByCreated = function(date, callback) {
	var query = {created: date};
	Lead.findOne(query).populate({
		path: 'followUps',
		select: 'date'
	}).exec(callback);
};

/*
module.exports.getLeadsByAssignment = function(username, callback) {
	Lead.find().populate({
		path: 'source',
		select: 'name'
	}).populate({
		path: 'assignment',
		match: {username: username},
		select: '-password'
	}).populate({
		path: 'status',
		select: 'name'
	}).populate({
		path: 'requirement',
		select: 'name'
	}).populate({
		path: 'followUps',
		select: 'date'
	}).exec(callback);
};
*/

module.exports.deleteLead = function(id, callback) {
	var query = {_id: id};
	Lead.findOneAndRemove(query, callback);
};

module.exports.updateLead = function(id, newLead, callback) {
	var query = {_id: id};
	Lead.findOneAndUpdate(query, newLead, callback);
};

module.exports.getLeads = function(callback) {
	Lead.find().populate({
		path: 'followUps',
		select: 'date'
	}).exec(callback);
};

module.exports.reset = function(callback) {
	Lead.remove({}, callback);
};

module.exports.resetLeadsUser = function(id, callback) {
	var query = {userid: id};

	Lead.remove(query, callback);
};

var https = require('https');
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var passport = require('passport');
var mongoose = require('mongoose');
var path = require('path');
var fs = require('fs');
var io = require('socket.io');

var ActiveUser = require('./models/ActiveUser');

var port = process.env.PORT | 3000;

var options = {
	key: fs.readFileSync(__dirname + '/security/leadmngmnt.pem'),
	cert: fs.readFileSync(__dirname + '/security/leadmngmnt.crt')
};

var dbConfig = require(__dirname+'/config/database');

var users = require(__dirname+'/routes/users');
var status = require(__dirname + '/routes/status');
var roles = require(__dirname + '/routes/roles');
var sources = require(__dirname + '/routes/source');
var leads = require(__dirname + '/routes/leads');
var requirements = require(__dirname + '/routes/requirements');
var followUps = require(__dirname + '/routes/followUp');
var forgotpass = require(__dirname + '/routes/forgot_pass');
var online = require(__dirname + '/routes/online');

//mongoose database connection
mongoose.connect(dbConfig.database);

//Connected event handler
mongoose.connection.on('connected', function(){
  console.log('Conneceted to Database '+ dbConfig.database);
});

//Mongoose event handler for error
mongoose.connection.on('error', function(err) {
  console.log('Database Error: '+err);
});

var app = express();

//CORS middleware
app.use(cors());

//Middleware for static assets
app.use('/img',express.static('public/img'));
app.use('/script', express.static('public/script'));
app.use('/css', express.static('public/css'));
app.use('/fonts', express.static('public/fonts'));

//Set static folder to public
app.use(express.static('public'));

//Body Parser middleware
app.use(bodyParser.json());

//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

require(__dirname+'/config/passport')(passport);

//Routes middleware for paths
app.use('/api/users', users);
app.use('/api/status', status);
app.use('/api/roles', roles);
app.use('/api/sources', sources);
app.use('/api/requirements', requirements);
app.use('/api/followups', followUps);
app.use('/api/leads', leads);
app.use('/api/forgetpass', forgotpass);
app.use('/api/online', online);

//Starting the server
var server = https.createServer(options, app);
io = io.listen(server);

io.on('connection', function(client) {
		client.on('disconnect', function() {
			ActiveUser.getUser(client.id, function(err, user) {
				if(err) throw err;

				ActiveUser.deleteUser(client.id, function(err){
					if(err) throw err;

					if(user){
						ActiveUser.getUsers(user.username, function(err, users) {
							if(err) throw err;
							if(users.length <= 0)
								io.sockets.emit('remove', {user: JSON.stringify(user)});
						});
					}
				});
			});
		});

		client.on('hi', function(message) {
			var data = JSON.parse(message);
			data.id = client.id;
			ActiveUser.getUser(client.id, function(err, user) {
				if(err) throw err;

				if(!user){
					user = new ActiveUser(data);
					ActiveUser.addUser(user, function(err, newUser) {
						if(err) throw err;
						io.sockets.emit('broadcast', {user: JSON.stringify(newUser)});
					});
				}
			});
		});
});

server.listen(3000, function() {
	console.log("Server started on port "+ port);
});

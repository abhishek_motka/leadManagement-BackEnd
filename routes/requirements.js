var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var auth = require('../scripts/authentication');

var Requirement = require('../models/Requirement');

var router = express.Router();

//Only logged in admin can access these routes

//Route to add new Requirement
router.post('/add', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){
	//Create Requirement object from the request body
	var newRequirement = new Requirement({
		name: req.body.name,
		status: Boolean(req.body.status)
	});

	//Check if Requirement with same name exists
	Requirement.getRequirementByName(newRequirement.name, function(err, requirement){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If Requirement exists, addition fails
		if(requirement) {
			return res.json({success: false, msg: 'This requirement already exists.'});
		}

		//If everything is ok then try to add the requirement
		Requirement.addRequirement(newRequirement, function(err, requirement) {

			//Failed due to server error
			if(err){
				return res.json({success: false, msg:'Failed to add new requirement. Same requirement may exist.'});
			}else{
				//Successfully registered
				return res.json({success: true, msg: 'New Requirement Added.'});
			}
		});
	});
});

//Route to update requirement
router.post('/update', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {

	//If name doesn't exist then can't update the requirement
	if(!req.body.updateName) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newRequirement = {};
	var name = req.body.updateName;

	//Create new object with request body parameters
	if(req.body.name)
		newRequirement.name = req.body.name;
	if(req.body.status !== undefined)
		newRequirement.status = req.body.status;

	//Update the Requirement in the database using a model
	Requirement.updateRequirement(name, newRequirement, function(err, updatedRequirement){
		if(err) {
			return res.json({success: false, msg: "Update Failed. Same requirement may exist."})
		}

		if(!updatedRequirement){
			return res.json({success: false, msg: 'Error in updating the Requirement.'});
		}else{
			return res.json({success: true, msg: 'Requirement updated successfully.'});
		}
	});
});

//Route to get Requirement by name
router.get('/get', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	if(!req.query.name){
		return res.json({success: false, msg: "Invalid request."});
	}

	var name = req.query.name;

	//Get Requirement by name from database
	Requirement.getRequirementByName(name, function(err, requirement){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!requirement){
			return res.json({success: false, msg: "Requirement not found."});
		}else{
			return res.json({success: true, requirement: requirement});
		}
	});
});

//Route to get all Requirements
router.get('/getAll', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
	//Get all Requirements from the database
	Requirement.getRequirements(function(err, requirements) {
		if(err) {
			res.status(500);
			return res.json({success: false, msg: "Server Error."});
		}

		if(!requirements) {
			return res.json({success: false, msg: "No requirements Found."});
		}else{
			return res.json({success: true, requirements: requirements});
		}
	});
});

//Route to delete Requirement from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	var name = req.body.name;

	Requirement.deleteRequirement(name, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Requirement."});
		else{
			return res.json({success: true, msg:"Requirement deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		Requirement.reset(function(err) {
			if(err){
				return res.status(401).json({success: false, msg: "Error in reseting Requirements."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

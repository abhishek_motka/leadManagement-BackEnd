var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var auth = require('../scripts/authentication');

var Source = require('../models/Source');

var router = express.Router();

//Only logged in admin can access these routes

//Route to add new Source
router.post('/add', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){
	//Create Source object from the request body
	var newSource = new Source({
		name: req.body.name,
		status: Boolean(req.body.status)
	});

	//Check if Source with same name exists
	Source.getSourceByName(newSource.name, function(err, source){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If Source exists, addition fails
		if(source) {
			return res.json({success: false, msg: 'This source already exists.'});
		}

		//If everything is ok then try to add the source
		Source.addSource(newSource, function(err, source) {
			res.type('application/json');

			//Failed due to server error
			if(err){
				return res.json({success: false, msg:'Failed to add new source.'});
			}else{
				//Successfully registered
				return res.json({success: true, msg: 'New Source Added.'});
			}
		});
	});
});

//Route to update source
router.post('/update', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {

	//If name doesn't exist then can't update the source
	if(!req.body.updateName) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newSource = {};
	var name = req.body.updateName;

	//Create new object with request body parameters
	if(req.body.name)
		newSource.name = req.body.name;
	if(req.body.status !== undefined)
		newSource.status = req.body.status;

	//Update the Source in the database using a model
	Source.updateSource(name, newSource, function(err, updatedSource){
		if(err) {
			return res.status(401).json({success: false, msg: "Something went Wrong. Same source may exist."});
		}

		if(!updatedSource){
			return res.json({success: false, msg: 'Error in updating the Source.'});
		}else{
			return res.json({success: true, msg: 'Source updated successfully.'});
		}
	});
});

//Route to get Source by name
router.get('/get', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	if(!req.query.name){
		return res.json({success: false, msg: "Invalid request."});
	}

	var name = req.query.name;

	//Get Source by name from database
	Source.getSourceByName(name, function(err, source){
		if(err) {
			return res.status(401).json({success: false, msg: "Server Error."});
		}

		if(!source){
			return res.json({success: false, msg: "Source not found."});
		}else{
			return res.json({success: true, source: source});
		}
	});
});

//Route to get all Sources
router.get('/getAll', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
	//Get all Sources from the database
	Source.getSources(function(err, sources) {
		if(err) {
			return res.status(401).json({success: false, msg: "Server Error."});
		}

		if(!sources) {
			return res.json({success: false, msg: "No sources Found."});
		}else{
			return res.json({success: true, sources: sources});
		}
	});
});

//Route to delete Source from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	var name = req.body.name;

	Source.deleteSource(name, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Source."});
		else{
			return res.json({success: true, msg:"Source deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		Source.reset(function(err) {
			if(err){
				return res.status(401).json({success: false, msg: "Error in reseting Sources."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

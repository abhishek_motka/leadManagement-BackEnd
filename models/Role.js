var mongoose = require('mongoose');

var RoleSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true,
		dropDups: true
	},
	status: {
		type: Boolean,
		required: true,
		default: true
	}
});

var Role = mongoose.model('Role', RoleSchema);

module.exports = Role;

module.exports.addRole = function(Role, callback) {
	Role.save(callback);
};

module.exports.getRoleById = function(id, callback) {
	Role.findById(id, callback);
};

module.exports.getRoleByName = function(name, callback) {
	var query = {name: name};
	Role.findOne(query, callback);
};

module.exports.deleteRole = function(name, callback) {
	var query = {name: name};
	Role.findOneAndRemove(query, callback);
};

module.exports.updateRole = function(name, newRole, callback) {
	var query = {name: name};
	Role.findOneAndUpdate(query, newRole, callback);
};

module.exports.getRoles = function(callback) {
	Role.find(callback);
};

module.exports.reset = function(callback) {
	Role.remove({}, callback);
};

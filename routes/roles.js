var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var auth = require('../scripts/authentication');

var Role = require('../models/Role');

var router = express.Router();

//Only logged in admin can access these routes

//Route to add new Role
router.post('/add', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){
	//Create Role object from the request body
	var newRole = new Role({
		name: req.body.name,
		status: Boolean(req.body.status)
	});

	//Check if role with same name exists
	Role.getRoleByName(newRole.name, function(err, role){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If role exists, addition fails
		if(role) {
			return res.json({success: false, msg: 'This role already exists.'});
		}

		//If everything is ok then try to add the role
		Role.addRole(newRole, function(err, role) {
			res.type('application/json');

			//Failed due to server error
			if(err){
				return res.json({success: false, msg:'Failed to add new role. Same role may exist.'});
			}else{
				//Successfully registered
				return res.json({success: true, msg: 'New Role Added.'});
			}
		});
	});
});

//Route to update role
router.post('/update', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {

	//If name doesn't exist then can't update the role
	if(!req.body.updateName) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newRole = {};
	var name = req.body.updateName;

	//Create new object with request body parameters
	if(req.body.name)
		newRole.name = req.body.name;
	if(req.body.status !== undefined)
		newRole.status = req.body.status;

	//Update the role in the database using a model
	Role.updateRole(name, newRole, function(err, updatedRole){
		if(err) {
			return res.status(401).json({success: false, msg: "Something went wrong. Same role may exist."});
		}

		if(!updatedRole){
			return res.json({success: false, msg: 'Error in updating the role.'});
		}else{
			return res.json({success: true, msg: 'Role updated successfully.'});
		}
	});
});

//Route to get role by name
router.get('/get', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	if(!req.query.name){
		return res.json({success: false, msg: "Invalid request."});
	}

	var name = req.query.name;

	//Get role by name from database
	Role.getRoleByName(name, function(err, role){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!role){
			return res.json({success: false, msg: "Role not found."});
		}else{
			return res.json({success: true, role: role});
		}
	});
});

//Route to get all roles
router.get('/getAll', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
	//Get all roles from the database
	Role.getRoles(function(err, roles) {
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!roles) {
			return res.json({success: false, msg: "No roles Found."});
		}else{
			return res.json({success: true, roles: roles});
		}
	});
});

//Route to delete role from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	var name = req.body.name;

	Role.deleteRole(name, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Role."});
		else{
			return res.json({success: true, msg:"Role deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		Role.reset(function(err) {
			if(err){
				return res.status(401).json({success: false, msg: "Error in reseting Roles."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

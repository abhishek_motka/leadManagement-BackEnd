var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../config/database');
var auth = require('../scripts/authentication');

var User = require('../models/user');
var Token = require('../models/tokens');

var router = express.Router();

//Route for REGISTER
//Only logged in admin can register new user.
router.post('/register', passport.authenticate('jwt', {session: false}),  auth.roleAuthorization(['admin']), function(req, res, next){

	//Create user object from the request body
	var newUser = new User({
		firstName: req.body.fName,
		lastName: req.body.lName,
		username: req.body.username,
		email: req.body.email,
		status: Boolean(req.body.status),
		type: req.body.type,
		role: req.body.role,
		password: req.body.password
	});

	//Check if user with same username exists
	User.getUserByUsername(newUser.username, function(err, user){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If user exists, registration fails
		if(user) {
			return res.json({success: false, msg: 'This username already exists.'});
		}

		//Check if user with same email address exists
		User.getUserByEmail(newUser.email, function(err, user){
			if(err) {
				return res.status(500).json({success: false, msg: "Server Error."});
			}

			//If exists, registration fails
			if(user) {
				return res.json({success: false, msg: "User with same email address exists."});
			}

			//If everything is ok then try to register the user
			User.addUser(newUser, function(err, user) {

				//Failed due to server error
				if(err){
					return res.json({success: false, msg:'Failed to register user. Same user may exist.'});
				}else{
					//Successfully registered
					return res.json({success: true, msg: 'User Registered'});
				}
			});
		});
	});
});

router.get('/validate', passport.authenticate('jwt', {session: false}), function(req, res, next){
	if(req.user.err)
		return res.status(401).json({success: false, msg: req.user.err});
	else
		return res.json({success: true});
});

//Route for Authentication
router.post('/authenticate', function(req, res, next){

	//Extract username and password from the request body
	var username = req.body.username;
	var password = req.body.password;

	//Find the user with the same username
	User.getUserByUsername(username, function(err, user){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If user not found, then login fails.
		if(!user){
			return res.json({success: false, msg: "Invalid Username or Password."});
		}

		//If user found, compare the actual password and the password sent in the request
		User.comparePassword(password, user.password, function(err, isMatch) {
				if(err) {
					return res.status(500).json({success: false, msg: "Server Error."});
				}

				//If matches then create a jwt token and send the token with userr information(without password)
				if(isMatch){
					var token = jwt.sign(user, config.secret, {
						expiresIn: 3600, //1 week
					});

					Token.removeToken(user.username, function(err){
						if(err) {
							return res.json({success: false, msg: "Error in session management."});
						}
					});

					res.json({
						success: true,
						token: 'JWT '+token,
						msg: "You are logged in.",
						user: {
							id: user._id,
							name: user.firstName + " " +user.lastName,
							email: user.email,
							status: user.status,
							username: user.username,
							type: user.type,
							role: user.role
						}
					});
				}else {
					return res.json({success: false, msg: "Invalid Username or Password."});
				}
		});
	});
});

//Route to update userprofile
router.post('/update', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	//If updateUsername doesn't exist then can't update the profile
	if(!req.body.updateUsername) {
		return res.json({success: false, msg: "Unable to update the user data."});
	}

	//Person can edit his/her profile only. Admin can update anyone's profile
	if(req.user.type === 'admin' || req.user.username === req.body.updateUsername){
		var newUser = {};
		var username = req.body.updateUsername;

		//Create new object with request body parameters
		if(req.body.fName)
			newUser.fName = req.body.fName;
		if(req.body.lName)
			newUser.lName = req.body.lName;
		if(req.body.status !== undefined)
			newUser.status = req.body.status;
		if(req.body.type && req.user.type === 'admin')
			newUser.type = req.body.type;
		if(req.body.username)
			newUser.username = req.body.username;
		if(req.body.email)
			newUser.email = req.body.email;
		if(req.body.role)
			newUser.role = req.body.role;

		//Update the user profile in the database using a model
		User.updateUser(username, newUser, function(err, updatedUser){
			if(err) {
				return res.json({success: false, msg: "Something went Wrong. Same user may exist."});
			}

			if(!updatedUser){
				return res.json({success: false, msg: 'Unable to update the user data.'});
			}else{
				return res.json({success: true, msg: 'Profile updated successfully.'});
			}
		});
	}else{
		return res.json({success: false, msg: 'Unauthorized attempt to update data.'});
	}
});

//route to update user password
router.post('/updatePassword', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	//If required parameters don't exist then invalid request
	if(!req.body.username || !req.body.newPassword || !req.body.password){
		return res.json({success: false, msg: 'Unable to update password'});
	}

	//User can update his/her password only
	if(req.user.username === req.body.username){

		User.getUserByUsername(req.body.username, function(err, user){
			if(err) {
				return res.status(500).json({success: false, msg: "Server Error."});
			}

			//If user with specified username doesn't exist then update fails
			if(!user){
				return res.json({success: false, msg: "Invalid username or password."});
			}else{
				//Compare oldpassword with the password in the request (must match)
				User.comparePassword(req.body.password, user.password, function(err, isMatch) {
					if(err) {
						return res.status(500).json({success: false, msg: "Server Error."});
					}

					if(!isMatch){
						return res.json({success: false, msg: "Old password doesn't match"});
					}else{
						//If old password matches then update the password
						User.updatePassword(req.body.username, req.body.newPassword, function(err, updatedUser){
							if(err) {
								return res.json({success: false, msg: "Error in updating password."});
							}

							if(!updatedUser){
								return res.json({success: false, msg:"Error in updating the password."});
							}else{
								var tokenNew = {
									username: req.body.username,
									exp: new Date().getTime()
								};

								Token.getTokenByUsername(tokenNew.username, function(err, token) {
									if(err)
										return res.status(500).json({success: false, msg: "Server Error"});

									if(!token){
										tokenNew = new Token(tokenNew);
										Token.addToken(tokenNew, function(err, token) {
											if(err) {
												return res.json({success: false, msg: "Something went wrong in session management."});
											}

											if(!token){
												return res.json({success: false, msg:"Error in session management."});
											}else{
												return res.json({success: true, msg:"Password updated successfully."});
											}
										});
									}else{
										Token.updateToken(tokenNew.username, token, function(err, tokenUpdated) {
											if(err)
												return res.status(500).json({success: false, msg: "Server Error."});

											if(!tokenUpdated)
												return res.json({success: false, msg: "Error in session management"});

											return res.json({success: true, msg:"Password updated successfully."});
										})
									}
								});
							}
						});
					}
				});
			}
		});
	}else{
		return res.json({success: false, msg: "You are not authorized to update the password."});
	}
});

//Route for Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), function(req, res, next){
	var username = "";
	//If username not specified in parameters then get profile of current user
	if(!req.query.username)
		username = req.user.username;
	else if(req.user.type === 'admin')	//Only admin is allowed to get other's profile
		username = req.query.username;
	else if(req.user.username !== req.query.username) //User can't see other user's profile
		return res.json({success: false, msg: "You are not authorized to view this profile."});

	//Get profile of user from database
	User.getUserByUsername(username, function(err, user){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}
		if(!user){
			return res.json({success: false, msg: "User not found."});
		}else{
			user.password = "Confidential";
			return res.json({success: true, user: user});
		}
	});
});

//Route for Profile
router.get('/get', passport.authenticate('jwt', {session: false}), function(req, res, next){
	var id = "";
	//If username not specified in parameters then get profile of current user
	if(!req.query.id)
		id = req.user.id;
	else if(req.user.type === 'admin')	//Only admin is allowed to get other's profile
		id = req.query.id;
	else if(req.user._id !== req.query.id) //User can't see other user's profile
		return res.json({success: false, msg: "You are not authorized to view this profile."});

	//Get profile of user from database
	User.getUserById(id, function(err, user){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}
		if(!user){
			return res.json({success: false, msg: "User not found."});
		}else{
			user.password = "Confidential";
			return res.json({success: true, user: user});
		}
	});
});

//Route to get all users registered
//Only admin is allowed to access this route
router.get('/getAll', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {

	User.getUsers(function(err, users) {
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!users) {
			return res.json({success: false, msg: "No user found."});
		}else {
			//Change password field to string: 'Confidential'
			var i = 0;
			for( ; i < users.length; i++) {
				users[i].password = "Confidential";
			}
			return res.json({success: true, users: users});
		}
	});
});

router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){
	if(!req.body.username || req.body.username === req.user.username){
		return res.json({success: false, msg: "Invalid delete request."});
	}

	var username = req.body.username;

	User.deleteUser(username, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the User."});
		else{
			var tokenNew = {
				username: username,
				exp: new Date().getTime()
			};

			Token.getTokenByUsername(username, function(err, token) {
				if(err)
					return res.status(500).json({success: false, msg: "Server Error"});

				if(!token){
					tokenNew = new Token(tokenNew);
					Token.addToken(tokenNew, function(err, token) {
						if(err) {
							return res.json({success: false, msg: "Something went wrong in session management."});
						}

						if(!token){
							return res.json({success: false, msg:"Error in session management."});
						}else{
							return res.json({success: true, msg:"User deleted successfully."});
						}
					});
				}else{
					Token.updateToken(username, tokenNew, function(err, tokenUpdated) {
						if(err)
							return res.status(500).json({success: false, msg: "Server Error."});

						if(!tokenUpdated)
							return res.json({success: false, msg: "Error in session management"});

						return res.json({success: true, msg:"User deleted successfully."});
					})
				}
			});
		}
	});
});

router.get('/logout', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	var tokenNew = {
		username: req.user.username,
		exp: new Date().getTime()
	};

	Token.getTokenByUsername(tokenNew.username, function(err, token) {
		if(err)
			return res.status(500).json({success: false, msg: "Server Error"});

		if(!token){
			tokenNew = new Token(tokenNew);
			Token.addToken(tokenNew, function(err, token) {
				if(err) {
					return res.json({success: false, msg: "Something went wrong in session management."});
				}

				if(!token){
					return res.json({success: false, msg:"Error in session management."});
				}else{
					return res.json({success: true, msg:"You are logged out."});
				}
			});
		}else{
			Token.updateToken(tokenNew.username, tokenNew, function(err, tokenUpdated) {
				if(err)
					return res.status(500).json({success: false, msg: "Server Error."});

				if(!tokenUpdated)
					return res.json({success: false, msg: "Error in session management"});

				return res.json({success: true, msg:"You are logged out."});
			})
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		User.reset(function(err) {
			if(err){
				return res.json({success: false, msg: "Error in reseting Users."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

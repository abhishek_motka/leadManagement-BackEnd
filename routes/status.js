var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var auth = require('../scripts/authentication');

var Status = require('../models/Status');

var router = express.Router();

//Only logged in admin can add access these routes

//Route to add new Status
router.post('/add', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){
	//Create status object from the request body
	var newStatus = new Status({
		name: req.body.name,
		order: req.body.order,
		status: Boolean(req.body.status)
	});

	//Check if status with same name exists
	Status.getStatusByName(newStatus.name, function(err, status){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		//If status exists, addition fails
		if(status) {
			return res.json({success: false, msg: 'This status already exists.'});
		}

		//Check if status with same order exists
		Status.getStatusByOrder(newStatus.order, function(err, status){
			if(err) {
				return res.status(500).json({success: false, msg: "Server Error."});
			}

			//If exists, addition fails
			if(status) {
				return res.json({success: false, msg: "Status with same order address exists."});
			}

			//If everything is ok then try to add the status
			Status.addStatus(newStatus, function(err, status) {
				res.type('application/json');

				//Failed due to server error
				if(err){
					return res.json({success: false, msg:'Failed to add new status. Same status may exist.'});
				}else{
					//Successfully registered
					return res.json({success: true, msg: 'New Status Added.'});
				}
			});
		});
	});
});

//Route to update status
router.post('/update', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
	//If name doesn't exist then can't update the status
	if(!req.body.updateName) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newStatus = {};
	var name = req.body.updateName;

	//Create new object with request body parameters
	if(req.body.name)
		newStatus.name = req.body.name;
	if(req.body.order)
		newStatus.order = req.body.order;
	if(req.body.status !== undefined)
		newStatus.status = req.body.status;

	//Update the status in the database using a model
	Status.updateStatus(name, newStatus, function(err, updatedStatus){
		if(err) {
			return res.json({success: false, msg: "Same status may exist."});
		}

		if(!updatedStatus){
			return res.json({success: false, msg: 'Error in updating the status.'});
		}else{
			return res.json({success: true, msg: 'Status updated successfully.'});
		}
	});
});

//Route to get status by name
router.get('/get', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	if(!req.query.name){
		return res.jaon({success: false, msg: "Invalid request."});
	}

	var name = req.query.name;

	//Get status by name from database
	Status.getStatusByName(name, function(err, status){
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!status){
			return res.json({success: false, msg: "Status not found."});
		}else{
			return res.json({success: true, status: status});
		}
	});
});

//Route to get all status
router.get('/getAll', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
	//Get all status from the database
	Status.getStatus(function(err, statuses) {
		if(err) {
			return res.status(500).json({success: false, msg: "Server Error."});
		}

		if(!statuses) {
			return res.json({success: false, msg: "No status Found."});
		}else{
			return res.json({success: true, status: statuses});
		}
	});
});

//Route to delete status from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	var name = req.body.name;

	Status.deleteStatus(name, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Status."});
		else{
			return res.json({success: true, msg:"Status deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		Status.reset(function(err) {
			if(err){
				return res.json({success: false, msg: "Error in reseting Statuses."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

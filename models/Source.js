var mongoose = require('mongoose');

var SourceSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true,
		dropDups: true
	},
	status: {
		type: Boolean,
		required: true,
		default: true
	}
});

var Source = mongoose.model('Source', SourceSchema);

module.exports = Source;

module.exports.addSource = function(Source, callback) {
	Source.save(callback);
};

module.exports.getSourceById = function(id, callback) {
	Source.findById(id, callback);
};

module.exports.getSourceByName = function(name, callback) {
	var query = {name: name};
	Source.findOne(query, callback);
};

module.exports.deleteSource = function(name, callback) {
	var query = {name: name};
	Source.findOneAndRemove(query, callback);
};

module.exports.updateSource = function(name, newSource, callback) {
	var query = {name: name};
	Source.findOneAndUpdate(query, newSource, callback);
};

module.exports.getSources = function(callback) {
	Source.find(callback);
};

module.exports.reset = function(callback) {
	Source.remove({}, callback);
};

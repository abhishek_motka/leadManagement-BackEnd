var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var auth = require('../scripts/authentication');

var Lead = require('../models/Lead');

var router = express.Router();

//Only logged in admin can access these routes

//Route to add new Follow Up
router.post('/add', passport.authenticate('jwt', {session: false}), function(req, res, next){

	//Create FollowUp object from the request body
	var newLead = new Lead({
		created: new Date().getTime(),
		fName: req.body.fName,
		lName: req.body.lName,
		mobile: req.body.mobile,
		address: req.body.address,
		source: req.body.src,
		assignment: req.body.assignment,
		userid: req.body.userid,
		status: req.body.status,
		requirement: req.body.req,
		notes: req.body.note,
		nextDate: req.body.ndate,
		followUps: req.body.followUps
	});


	//If everything is ok then try to add the follow up
	Lead.addLead(newLead, function(err, lead) {
		res.type('application/json');

		//Failed due to server error
		if(err){
			res.json({success: false, msg:'Failed to add new lead.'});
		}else{
			//Successfully added
			if(lead)
				return res.json({success: true, msg: 'New Lead Added.', lead: lead});
			else {
				return res.json({success: false, msg: "Error in creating new lead."});
			}
		}
	});
});

//Route to update lead
router.post('/update', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']),function(req, res, next) {

	//If createdDate doesn't exist then can't update the lead
	if(!req.body.id) {
		return res.json({success: false, msg: "Invalid update request."});
	}

	var newLead = {};
	var id = req.body.id;

	//Create new object with request body parameters
	if(req.body.fName)
		newLead.fName = req.body.fName;
	if(req.body.lName)
		newLead.lName = req.body.lName;
	if(req.body.mobile)
		newLead.mobile = req.body.mobile;
	if(req.body.address)
		newLead.address = req.body.address;
	if(req.body.src)
		newLead.source = req.body.src;
	if(req.body.assignment)
		newLead.assignment = req.body.assignment;
	if(req.body.status)
		newLead.status = req.body.status;
	if(req.body.requirement)
		newLead.requirement = req.body.req;
	if(req.body.notes)
		newLead.notes = req.body.note;
	if(req.body.followUps)
		newLead.followUps = req.body.followUps;
	if(req.body.userid)
		newLead.userid = req.body.userid;
	if(req.body.ndate)
		newLead.nextDate = req.body.ndate;

	//Update the lead in the database using a model
	Lead.updateLead(id, newLead, function(err, updatedLead){
		if(err) {
			return res.json({success: false, msg: "Something went wrong."});
		}

		if(!updatedLead){
			return res.json({success: false, msg: 'Error in updating the lead.'});
		}else{
			return res.json({success: true, msg: 'Lead updated successfully.', lead: updatedLead});
		}
	});
});

//Route to get lead by id
router.get('/get', passport.authenticate('jwt', {session: false}), function(req, res, next){

	if(!req.query.id){
		return res.json({success: false, msg: "Invalid request."});
	}

	var id = req.query.id;

	//Get Lead by id from database
	Lead.getLeadById(id, function(err, lead){
		if(err) {
			return res.json({success: false, msg: "Server Error."});
		}

		if(!lead){
			return res.json({success: false, msg: "Lead not found."});
		}else{
			return res.json({success: true, lead: lead});
		}
	});
});

//Route to get lead by created
router.get('/getByDate', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	if(!req.query.created){
		return res.json({success: false, msg: "Invalid request."});
	}

	var date = req.query.created;

	//Get lead by created date from database
	Lead.getLeadByCreated(date, function(err, lead){
		if(err) {
			return res.json({success: false, msg: "Server Error."});
		}

		if(!lead){
			return res.json({success: false, msg: "Lead not found."});
		}else{
			return res.json({success: true, lead: lead});
		}
	});
});

//Route to get lead by username
router.get('/getByUser', passport.authenticate('jwt', {session: false}), function(req, res, next){

	if(!req.user.userid){
		return res.json({success: false, msg: "Invalid request."});
	}

	var userid = req.user.userid;

	//Get lead by assigned user from database
	Lead.getLeadsByUser(userid, function(err, lead){
		if(err) {
			return res.json({success: false, msg: "Server Error."});
		}

		if(!lead){
			return res.json({success: false, msg: "Lead not found."});
		}else{
			return res.json({success: true, leads: lead});
		}
	});
});

//Route to get lead by id
router.get('/getById', passport.authenticate('jwt', {session: false}), function(req, res, next){

	var id = req.user.id;

	//Get Lead by id from database
	Lead.getLeadById(id, function(err, lead){
		if(err) {
			return res.json({success: false, msg: "Server Error."});
		}

		if(!lead){
			return res.json({success: false, msg: "Lead not found."});
		}else{
			return res.json({success: true, lead: lead});
		}
	});
});

//Route to get all leads
router.get('/getAll', passport.authenticate('jwt', {session: false}), function(req, res, next) {
	//Get all leads from the database
	if(req.user.type.toLowerCase() === 'admin'){
		Lead.getLeads(function(err, leads) {
			if(err) {
				return res.json({success: false, msg: "Server Error."});
			}

			if(!leads) {
				return res.json({success: false, msg: "No Lead Found."});
			}else{
				return res.json({success: true, leads: leads});
			}
		});
	}else if(req.user.type.toLowerCase() === 'user'){
		Lead.getLeadsByUser(req.user._id, function(err, lead){
			if(err) {
				return res.json({success: false, msg: "Server Error."});
			}

			if(!lead){
				return res.json({success: false, msg: "Lead not found."});
			}else{
				return res.json({success: true, leads: lead});
			}
		});
	}else{
		return res.json({success: false, msg: "You are not authorized to access this path."});
	}
});

//Route to delete follow up from the database
router.delete('/delete', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next){

	var id = req.body.id;

	Lead.deleteLead(id, function(err){
		if(err)
			return res.json({success: false, msg:"Not able to delete the Lead."});
		else{
			return res.json({success: true, msg:"Lead deleted successfully."});
		}
	});
});

//Route to reset the collection
router.delete('/reset', passport.authenticate('jwt', {session: false}), auth.roleAuthorization(['admin']), function(req, res, next) {
		Lead.reset(function(err) {
			if(err){
				return res.json({success: false, msg: "Error in reseting leads."});
			}else{
				return res.json({success: true, msg: "All data is reset."});
			}
		})
});

module.exports = router;

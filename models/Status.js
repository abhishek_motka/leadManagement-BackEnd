var mongoose = require('mongoose');
var dbConfig = require('../config/database');

var StatusSchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true,
		dropDups: true
	},
	status: {
		type: Boolean,
		required: true,
		default: true
	},
	order: {
		type: Number,
		required: true,
		unique: true,
		dropDups: true
	}
});

var Status = mongoose.model('Status', StatusSchema, 'status');

module.exports = Status;

module.exports.addStatus = function(status, callback) {
	status.save(callback);
};

module.exports.getStatusById = function(id, callback) {
	Status.findById(id, callback);
};

module.exports.getStatusByName = function(name, callback) {
	var query = {name: name};
	Status.findOne(query, callback);
};

module.exports.getStatusByOrder = function(order, callback) {
	var query = {order: order};
	Status.findOne(query, callback);
};

module.exports.deleteStatus = function(name, callback) {
	var query = {name: name};
	Status.findOneAndRemove(query, callback);
};

module.exports.updateStatus = function(name, newStatus, callback) {
	var query = {name: name};
	Status.findOneAndUpdate(query, newStatus, callback);
};

module.exports.getStatus = function(callback) {
	Status.find(callback);
};

module.exports.reset = function(callback) {
	Status.remove({}, callback);
};

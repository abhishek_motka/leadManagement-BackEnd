var mongoose = require('mongoose');

var ActiveUserSchema = mongoose.Schema({
  id: {
    type: String,
    unique: true,
    required: true
  },
	email: {
		type: String,
		required: true
	},
  name: {
    type: String,
    required: true
  },
  username: {
		type: String,
		required: true
	}
});

var ActiveUser = mongoose.model('ActiveUser', ActiveUserSchema);

module.exports = ActiveUser;

module.exports.addUser = function(activeUser, callback) {
	activeUser.save(callback);
};

module.exports.deleteUser = function(id, callback) {
	var query = {id: id};
	ActiveUser.findOneAndRemove(query, callback);
};

module.exports.getUser = function(id, callback) {
  var query = {id: id};
  ActiveUser.findOne(query, callback);
};

module.exports.getUsers = function(username, callback) {
  var query = {username: username};
  ActiveUser.find(query, callback);
};

module.exports.getAll = function(callback) {
	ActiveUser.find({}, callback);
};

var passport = require('passport');
var express = require('express');
var jwt = require('jsonwebtoken');

var ActiveUser = require('../models/ActiveUser');

var router = express.Router();

router.get('/get', passport.authenticate('jwt', {session: false}), function(req, res, next){
  ActiveUser.getAll(function(err, users) {
    if(err) throw err;
    var data = {};
    var result = [];
 
    for(var i = 0; i < users.length; i++){
      if(!(users[i].username in data)) {
        data[users[i].username] = '';
        delete users[i].id;
        result.push(users[i]);
      }
    }

    return res.json({success: true, users: result});
  });
});

module.exports = router;
